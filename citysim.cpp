#include <iostream>
#include <iomanip>
#include "utils.h"
using namespace std;

const char *fname = "/home/cs302/labs/lab5/citylist.txt";

int main(int argc, char *argv[])
{
    // instantiate a citysim
    citysim c;
    int pop = 0;

    // load the city data from the text file
    c.load_cities(fname);

    // calc population
    for (size_t i = 0; i < c.city_v.size(); ++i)
        pop += c.city_v[i]->get_pop();
    cout << "FULL CITY LIST (N=" << c.city_v.size() << " population=" << pop 
         << "):\n\n";
    // iterate through city vector and print
    for (size_t i = 0; i < c.city_v.size(); ++i)
        cout << setw(3) << right << i << " " << *c.city_v[i] << endl;

    // iterate through city categories, print the indexes in the city vector that
    // belong to each one
    for (int cat = 0; cat < catcount; ++cat) {
        pop = 0;
        for (size_t i = 0; i < c.by_category[cat].size(); ++i)
            pop += c.city_v[c.by_category[cat][i]]->get_pop();
        cout << "\n" << catname[cat] << " CITY LIST (N="
             << c.by_category[cat].size() << " population=" << pop << "):\n\n";
        for (size_t i = 0; i < c.by_category[cat].size(); ++i)
            cout << setw(3) << right << c.by_category[cat][i] << " "
                 << *c.city_v[c.by_category[cat][i]] << endl;
    }

    // print out the distance table
    cout << "\nDISTANCE table:\n\n";
    for(size_t i = 0; i < c.city_v.size(); ++i)
        for (size_t j = 0; j <= i; ++j)
            cout << setw(3) << i << " " << c.city_v[i]->get_name() << " to " 
                 << c.city_v[j]->get_name() << " " << c.distances(i,j) << endl;
    return 0;
}
