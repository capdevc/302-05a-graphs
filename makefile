
CC = g++
CFLAGS = -g -Wall -std=c++0x
BIN = citysim

OBJ = $(BIN:%=%.o) utils.o 

all: $(BIN)

citysim: $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

.cpp.o: 
	$(CC) $(CFLAGS) -c $<

clean:
	@rm -f $(BIN) $(OBJ)
	@rm -f cityinfo.txt
	@rm -f citygraph.txt
