#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstring>
#include <cmath>
#include "utils.h"
using namespace std;

#define DEG2RAD (M_PI/180.0)
#define RAD2DEG (180.0/M_PI)

// print out city data on one line
ostream& operator<<(ostream &out, const city &c)
{
    out << setw(20) << left << c.name
        << setw(10) << left << catname[c.cat]
        << setw(9) << right << c.pop
        << setw(8) << setprecision(2) << fixed << right << c.lat
        << setw(8) << setprecision(2) << fixed << right << c.lng;
    return out;
}

// city class constructor
city::city(string name, category cat, double lat, double lng, int pop)
    : name(name), cat(cat), lat(lat), lng(lng), pop(pop)
{}

// calculate distance from this to other
const double city::distance_to(const city &other) const
{
    double dt = (DEG2RAD/2) * (lat - other.lat);
    double dg = (DEG2RAD/2) * (lng - other.lng);
    return 2*3982*asin(sqrt(pow(sin(dt), 2) + cos(lat*DEG2RAD)*cos(other.lat*DEG2RAD)*pow(sin(dg), 2)));
}

// smatrix constructor, defaults to a 1x1 matrix
smatrix::smatrix() : dim(1)
{
    l = 2;
    data = new double[2];
}

// smatrix destructor, frees the data
smatrix::~smatrix()
{
    delete[] data;
}

// resize/clear the matrix.
void smatrix::reset(const int newdim)
{
    // calculate new array size
    l = newdim*(newdim+1)/2;
    dim = newdim;
    delete[] data; // allocate array
    data = new double[l]; // free the old data
}

// take a row, column pair and index into the array
double& smatrix::operator()(const int i, const int j)
{
    // index into our array;
    if (j < i)
        return data[(i*(i+1))/2 + j];
    else
        return data[(j*(j+1))/2 + i];
}

// load in the city data and calc distances
void citysim::load_cities(const char *fname)
{
    char line[1024], namebuf[256], catbuf[256];
    float latbuf, lngbuf;
    int popbuf;
    int index = 0;
    category cat;

    for (size_t i = 0; i < (size_t) catcount; ++i)
        by_category.push_back(vector<int>());
    FILE *infile = fopen(fname, "r");
    if (infile != NULL)
        while (fgets(line, 1024, infile) != NULL) {
            if (strncmp(line, "#", 1)) { // skip #
                sscanf(line, "%s %*s %s %f %f %d", namebuf, catbuf, &latbuf, &lngbuf, &popbuf);
                for (int i = 0; i < catcount; i++) // parse the category
                    if (!strncmp(catbuf, catname[(category)i].c_str(), 256)) cat = (category)i;
                city_v.push_back(new city(string(namebuf), cat, latbuf, lngbuf, popbuf));
                by_category[cat].push_back(index++); // add the city_v index to the right cat vector
            }
        }
    // set the distance matrix to the right size
    distances.reset(city_v.size());
    // calculate the distances
    for (size_t i = 0; i < city_v.size(); ++i) {
        for (size_t j = 0; j < i; ++j)
            distances(i,j) = city_v[i]->distance_to(*city_v[j]);
        distances(i,i) = 0; // 0 for the diagonal
    }
}
