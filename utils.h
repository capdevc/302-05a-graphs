#ifndef UTILS_H_DOIQPVDA
#define UTILS_H_DOIQPVDA

#include <string>
#include <vector>
using namespace std;

typedef enum {LOCAL, REGIONAL, GLOBAL} category;
const int catcount = 3;
const string catname[3] = {"LOCAL", "REGIONAL", "GLOBAL"};

class city {
    friend ostream &operator<<(ostream &, const city &);
    const string name;
    const category cat;
    const double lat, lng;
    const int pop;
public:
    city(string name, category cat, double lat, double lng, int pop);
    ~city();
    const double distance_to(const city &other) const;
    const int get_pop() const {
        return pop;
    }
    const string& get_name() const {
        return name;
    }
};
ostream &operator<<(ostream &, const city &);

// symmetric matrix class for triangular matrices
class smatrix {
    double *data; // array for the actual data
    int dim; // matrix row length
    size_t l; // length of the array
public:
    smatrix();
    ~smatrix();
    double &operator()(const int i, const int j); // access Mij
    void reset(const int newdim); // resize the matrix
};

class citysim {
public:
    citysim() {}
    vector<city *> city_v; // vector of city data
    // 2D array of city_v indexes in each category
    vector<vector<int> > by_category;
    smatrix distances; // distances matrix
    void load_cities(const char *fname);
};

#endif /* end of include guard: UTILS_H_DOIQPVDA */
